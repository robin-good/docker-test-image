FROM elixir:1.6-slim

RUN apt-get update \
    && apt-get install -y git build-essential \
    && mix local.hex --force \
    && mix local.rebar --force